import React from "react";
import FileCell from "./FileCell"

function FileGridContainer(props) {
    // Create file list.
    const filesToRender = props.fileList.map(file => (
        <FileCell fileInfo={file} allFiles={props.fileList} setFiles={props.setFiles}></FileCell>
    ));

    return <div className="fileGridContainer">
        {filesToRender}
    </div>
}

export default FileGridContainer