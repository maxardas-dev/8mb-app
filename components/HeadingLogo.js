import React, {useRef, useState} from 'react';
import {Canvas, useFrame} from '@react-three/fiber'

class FileBox extends React.Component {
    render() {
        // Top front left directionalLight.
        return (
            <div className='headingContainer'>
                <div className='logoContainer'>
                    <Canvas>
                        <directionalLight position={[-0.5, 1, 0.5]} intensity={10}/>
                        <Box position={[0, 0, 0]} />
                    </Canvas>
                </div>
                <h1 className='mainHeading'>8MB</h1>
            </div>
        )
    }
}

function Box(props) {
    // This reference will give us direct access to the mesh
    const mesh = useRef()
    // Set up state for the hovered and active state
    const [hovered, setHover] = useState(false)
    const [active, setActive] = useState(false)
    // Subscribe this component to the render-loop, rotate the mesh every frame
    useFrame((state, delta) => (mesh.current.rotation.x += 0.01, mesh.current.rotation.y += 0.01))
    // Return view, these are regular three.js elements expressed in JSX
    return (
        <mesh
        {...props}
        ref={mesh}
        scale={3}
        onClick={(event) => setActive(!active)}
        onPointerOver={(event) => setHover(true)}
        onPointerOut={(event) => setHover(false)}
        >
        <boxGeometry args={[1, 1, 1]} />
        <meshStandardMaterial color={hovered ? 'blue' : 'darkblue'} />
    </mesh>
  )
}

export default FileBox;