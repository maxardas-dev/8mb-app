import React, {useEffect, useRef, useState} from "react";
import {Canvas, useFrame} from '@react-three/fiber'
import {useSpring, animated, config} from '@react-spring/three'

function FileCell(props) {
    function removeFile (e) {
        const filteredFiles = props.allFiles.filter((file) => file.name !== props.fileInfo.name)
        props.setFiles(filteredFiles)
        e.stopPropagation()
    }
    
    function handleDivClick(e) {
        e.stopPropagation();
    }

    return <div className="fileCell" onClick={handleDivClick}>
        <button onClick={removeFile} className="fileDeleteButton">X</button>
        <div className="fileCubeContainer">
            <Canvas>
                <directionalLight position={[0, -0.1, 0.5]} intensity={2}/>
                <FileCube/>
            </Canvas>
        </div>
        <p>{props.fileInfo.name}</p>
        <p>{props.fileInfo.type}</p>
    </div>
}

function FileCube(props) {
    const mesh = useRef();
    const [loaded, setLoaded] = useState(false)

    const setRotation = () => {
        mesh.current.rotation.x = 0.6
    }

    const {scale} = useSpring({
        scale: loaded ? 2 : 0,
        config: config.wobbly
    });

    useEffect(() => {
        setLoaded(true)
    })
    
    return <animated.mesh
        {...props}
        ref={mesh}
        scale={scale}
        onBeforeRender={setRotation}
        >

        <boxGeometry args={[1, 1, 1.2]} />
        <meshStandardMaterial color={'darkblue'} />
    </animated.mesh>
}

export default FileCell;