import React, {useMemo, useState} from 'react';
import {useDropzone} from 'react-dropzone'
import FileGridContainer from './FileGridContainer';

const baseStyle = {
    width: '80%',
    height: 'auto',
    margin: 'auto',
    fontFamily: 'Helvetica',
    borderStyle: 'solid',
    transition: 'border .3s ease-in-out',
    textAlign: 'center',
    borderRadius : '20px'
}

const focusStyle = {
    borderColor: '#FFFFFF'
}

const acceptStyle = {
    borderColor: '#E8F50E'
}

const rejectStyle = {
    borderColor: '#FF1744'
}

function FileBox(props) {

    const [filesList, setFiles] = useState([])

    const {getRootProps, getInputProps, isFocused, isDragAccept, isDragReject} = useDropzone({
        onDrop: newFiles => setFiles(prevState => [...prevState, ...newFiles]),
    });

    // Conditional rendering if no files.
    let FileBoxContents = [];

    if (filesList.length === 0) {
        FileBoxContents = <p className='bodyTextWhite'>Drop your files here!</p>
    } else {
        FileBoxContents = <FileGridContainer fileList={filesList} setFiles={setFiles}/>
    }

    // Determine style
    const style = useMemo(() => ({
        ...baseStyle,
        ...(isFocused ? focusStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
      }), [
        isFocused,
        isDragReject,
        isDragAccept
      ]);

    return (
        <section>
            <div {...getRootProps({style})}>
                <input {...getInputProps()} />
                {FileBoxContents}
            </div>
        </section>
    );
}

export default FileBox; 