// _app.js
import '../styles/styles.css'

const MyApp = ({ Component, pageProps }) => {
  return <div className='page'> 
    <Component {...pageProps}/>
  </div>
}
export default MyApp