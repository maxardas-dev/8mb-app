import Head from 'next/head'
import FileBox from '../components/FileBox'
import HeadingLogo from '../components/HeadingLogo'

export default function Home() {
  return (
    <div>
      <Head>
        <title>8MB</title>
        <meta name="description" content="A nostalgic trip into cloud storage." />
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>

      <main>
        <HeadingLogo></HeadingLogo>
        <FileBox></FileBox>
      </main>

      <footer>
      
      </footer>
    </div>
  )
}
