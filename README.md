## 8MB

8MB is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`] (https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

The aim of this project is to create a cloud storage platform with the look and feel of the PlayStation 2 memory card screen, while also forcing users to work within the storage constraints of the console's classic 8MB memory cards.

This project primarily utilises:
- [React](https://reactjs.org/)
- [Three.js](https://threejs.org/)
- [React Three Fiber](https://github.com/pmndrs/react-three-fiber)
- [React Dropzone](https://react-dropzone.js.org/)

## Getting Started

First, install dependencies:
```bash
npm install
```

Then, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
